/* eslint-disable max-len */
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const permissionMiddleware = require('../middlewares/permissionMiddleware');

const {
  addTruck,
  assignTruck,
  getUserTruckById,
  getUserTrucks,
  updateUserTruckById,
  deleteUserTruckById,
} = require('../controllers/truckController');

router.post('/', authMiddleware, permissionMiddleware, addTruck);
router.post('/:truck_id/assign', authMiddleware, permissionMiddleware, assignTruck);
router.get('/:truck_id', authMiddleware, permissionMiddleware, getUserTruckById);
router.get('/', authMiddleware, permissionMiddleware, getUserTrucks);
router.put('/:truck_id', authMiddleware, permissionMiddleware, updateUserTruckById);
router.delete('/:truck_id', authMiddleware, permissionMiddleware, deleteUserTruckById);

// eslint-disable-next-line eol-last
module.exports = router;