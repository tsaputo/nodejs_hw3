const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require('../controllers/userController');

router.get('/me', authMiddleware, getProfileInfo);
router.delete('/me', authMiddleware, deleteProfile);
router.patch('/me/password', authMiddleware, changeProfilePassword);

module.exports = router;
