/* eslint-disable max-len */
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const permissionMiddleware = require('../middlewares/permissionMiddleware');

const {
  addLoad,
  getUserLoadById,
  postUserLoadById,
  getAllLoads,
  deleteUserLoadById,
  updateUserLoadById,
  getActiveLoads,
  setNextState,
  getShippingInfo,
} = require('../controllers/loadController');

router.post('/', authMiddleware, permissionMiddleware, addLoad);
router.patch('/active/state', authMiddleware, permissionMiddleware, setNextState);
router.get('/active', authMiddleware, permissionMiddleware, getActiveLoads);
router.get('/:load_id/shipping_info', authMiddleware, permissionMiddleware, getShippingInfo);
router.get('/:load_id', authMiddleware, permissionMiddleware, getUserLoadById);
router.get('/', authMiddleware, permissionMiddleware, getAllLoads);
router.post('/:load_id/post', authMiddleware, permissionMiddleware, postUserLoadById);
router.delete('/:load_id', authMiddleware, permissionMiddleware, deleteUserLoadById);
router.put('/:load_id', authMiddleware, permissionMiddleware, updateUserLoadById);

// eslint-disable-next-line eol-last
module.exports = router;