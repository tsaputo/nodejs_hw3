const jwt = require('jsonwebtoken');
const user = require('../models/user');
require('dotenv').config();

module.exports = (request, response, next) => {
  const accessRoles = {
    DRIVER: {
      POST: ['/api/trucks', 'truck_assign'],
      GET: [
        '/api/loads',
        '/api/trucks',
        '/api/loads/active',
        'load_with_id',
        'truck_with_id',
      ],
      PATCH: ['/api/loads/active/state'],
      DELETE: ['truck_delete'],
      PUT: ['truck_update'],
    },
    SHIPPER: {
      POST: ['/api/loads', 'post'],
      GET: ['/api/loads', 'load_with_id', 'shipping_info'],
      PATCH: [],
      DELETE: ['load_delete'],
      PUT: ['load_update'],
    },
  };
  const token = request.headers['authorization'];
  const payload = jwt.verify(token, process.env.JWT_SECRET);
  let requestUrl = request.originalUrl;
  const method = request.method;
  const LOAD_URL_PATTERN = /^\/api\/loads\/[a-fA-F0-9]{24}$/;
  const TRUCK_URL_PATTERN = /^\/api\/trucks\/[a-fA-F0-9]{24}$/;

  if (requestUrl.includes('post')) {
    requestUrl = 'post';
  }

  if (TRUCK_URL_PATTERN.test(requestUrl) && method === 'GET') {
    requestUrl = 'truck_with_id';
  }

  if (TRUCK_URL_PATTERN.test(requestUrl) && method === 'PUT') {
    requestUrl = 'truck_update';
  }

  if (TRUCK_URL_PATTERN.test(requestUrl) && method === 'DELETE') {
    requestUrl = 'truck_delete';
  }

  if (requestUrl.includes('assign')) {
    requestUrl = 'truck_assign';
  }

  if (requestUrl.includes('shipping_info')) {
    requestUrl = 'shipping_info';
  }

  if (LOAD_URL_PATTERN.test(requestUrl) && method === 'DELETE') {
    requestUrl = 'load_delete';
  }

  if (LOAD_URL_PATTERN.test(requestUrl) && method === 'PUT') {
    requestUrl = 'load_update';
  }

  if (LOAD_URL_PATTERN.test(requestUrl) && method === 'GET') {
    requestUrl = 'load_with_id';
  }

  user
      .findById(payload._id)
      .exec()
      .then((user) => {
        if (accessRoles[user.role][method].includes(requestUrl)) {
          next();
        } else
        // eslint-disable-next-line brace-style
        {
          response
              .status(401)
              .json({status: 'This action is not allowed for you'});
        }
      })
      .catch((error) => {
        response.status(500).json({message: error.message});
      });
};
