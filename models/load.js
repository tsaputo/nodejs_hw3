const mongoose = require('mongoose');
const {Schema} = mongoose;

module.exports = mongoose.model(
    'load',
    new Schema({
      name: {
        required: true,
        type: String,
      },
      userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      assigneeId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      logs: [
        {
          message: String,
          timestamp: {
            type: Date,
            default: Date.now,
          },
        },
      ],
      status: {
        required: true,
        type: String,
        default: 'NEW',
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
      },
      state: {
        type: String,
        enum: [
          'En route to Pick Up',
          'Arrived to Pick Up',
          'En route to delivery',
          'Arrived to delivery',
        ],
      },
      dimensions: {
        width: Number,
        length: Number,
        height: Number,
      },
      payload: {
        required: true,
        type: Number,
      },
      pickup_address: {
        required: true,
        type: String,
      },
      delivery_address: {
        required: true,
        type: String,
      },
      createdDate: {
        type: Date,
        default: Date.now,
      },
      truckId: {
        type: Schema.Types.ObjectId,
        ref: 'Truck',
      },
    }),
);
