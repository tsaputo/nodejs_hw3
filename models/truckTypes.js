module.exports.truckTypes = [
  {
    type: 'SPRINTER',
    payload: 1700,
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
  },
  {
    type: 'SMALL STRAIGHT',
    payload: 2500,
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
  },
  {
    type: 'LARGE STRAIGHT',
    payload: 4000,
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
  },
];
