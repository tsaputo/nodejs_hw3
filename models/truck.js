const mongoose = require('mongoose');
const {Schema} = mongoose;

module.exports = mongoose.model(
    'truck',
    new Schema({
      userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      assigneeId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      type: {
        required: true,
        type: String,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIT'],
      },
      status: {
        required: true,
        type: String,
        default: 'OS',
        enum: ['OS', 'IS', 'OL'],
      },
      createdDate: {
        type: Date,
        default: Date.now,
      },
    }),
);
