const mongoose = require('mongoose');
const {Schema} = mongoose;

module.exports = mongoose.model(
    'user',
    new Schema({
      email: {
        required: true,
        type: String,
        unique: true,
      },
      password: {
        required: true,
        type: String,
      },
      createdDate: {
        type: Date,
        default: Date.now,
      },
      role: {
        required: true,
        type: String,
        enum: ['DRIVER', 'SHIPPER'],
      },
    }),
);
