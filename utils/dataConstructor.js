module.exports.loadObj = function(load) {
  return {
    load: {
      _id: load._id,
      created_by: load.userId,
      assigned_to: load.assigneeId,
      status: load.status,
      state: load.state,
      name: load.name,
      payload: load.payload,
      pickup_address: load.pickup_address,
      delivery_address: load.delivery_address,
      dimensions: load.dimensions,
      logs: load.logs,
      created_date: load.createdDate,
    },
  };
};

module.exports.truckObj = function(truck) {
  return {
    truck: {
      _id: truck._id,
      created_by: truck.userId,
      assigned_to: truck.assigneeId,
      type: truck.type,
      status: truck.status,
      created_date: truck.createdDate,
    },
  };
};

module.exports.shippingObj = function(load, truck) {
  if (!truck) {
    return {
      load: {
        _id: load._id,
        created_by: load.userId,
        assigned_to: load.assigneeId,
        status: load.status,
        state: load.state,
        name: load.name,
        payload: load.payload,
        pickup_address: load.pickup_address,
        delivery_address: load.delivery_address,
        dimensions: load.dimensions,
        logs: load.logs,
        created_date: load.createdDate,
      },
      truck: null,
    };
  } else {
    return {
      load: {
        _id: load._id,
        created_by: load.userId,
        assigned_to: load.assigneeId,
        status: load.status,
        state: load.state,
        name: load.name,
        payload: load.payload,
        pickup_address: load.pickup_address,
        delivery_address: load.delivery_address,
        dimensions: load.dimensions,
        logs: load.logs,
        created_date: load.createdDate,
      },
      truck: {
        _id: truck._id,
        created_by: truck.userId,
        assigned_to: truck.assigneeId,
        type: truck.type,
        status: truck.status,
        created_date: truck.createdDate,
      },
    };
  }
};
