const {truckTypes} = require('../models/truckTypes');

module.exports.defineTypes = function(payload, dimensions) {
  const resultArr = truckTypes
      .filter(
          (truckType) =>
            truckType.payload > 100 &&
        truckType.dimensions.width > 50 &&
        truckType.dimensions.length > 50 &&
        truckType.dimensions.height > 50,
      )
      .map((item) => item.type);

  return resultArr;
};

module.exports.defineNextState = function(currentState, status) {
  if (currentState === 'En route to Pick Up') {
    state = 'Arrived to Pick Up';
  } else if (currentState === 'Arrived to Pick Up') {
    state = 'En route to delivery';
  } else if (currentState === 'En route to delivery') {
    state = 'Arrived to delivery';
    status = 'SHIPPED';
  }
  return {state, status};
};

module.exports.addLogsToLoad = async (shema, id, message) => {
  await shema.findOneAndUpdate(
      {_id: id},
      {
        $push: {
          logs: {
            message: message,
            time: Date.now,
          },
        },
      },
  );
};
