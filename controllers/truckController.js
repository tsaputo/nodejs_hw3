/* eslint-disable camelcase */
const Truck = require('../models/truck');
const mongoose = require('mongoose');

module.exports.addTruck = (request, response) => {
  const {type} = request.body;
  const userId = request.user._id;
  const truck = new Truck({userId, type, assigneeId: null});
  truck
      .save()
      .then(() => {
        response.status(200).json({message: 'Truck created successfully'});
      })
      .catch((error) => {
        response.status(400).json({message: error.message});
      });
};

module.exports.deleteUserTruckById = (request, response) => {
  const {truck_id} = request.params;

  const isValid = mongoose.Types.ObjectId.isValid(truck_id);
  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }

  const query = Truck.where({userId: request.user._id});
  query
      .findOne()
      .where({
        $and: [{_id: truck_id}, {$or: [{status: 'OS'}, {status: 'IS'}]}],
      })
      .exec()
      .then((truck) => {
        if (!truck) {
          return response.status(400).json({
            status:
            'There is no truck with such id or status is ONLOAD! Try again!',
          });
        }
        if (truck.status === 'OL') {
          return response
              .status(400)
              .json({status: 'You cannot remove truck ONLOAD'});
        }
        truck.remove();
        response.status(200).json({message: 'Truck deleted successfully'});
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.assignTruck = (request, response) => {
  const {truck_id} = request.params;
  const userId = request.user._id;

  const isValid = mongoose.Types.ObjectId.isValid(truck_id);
  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }

  const query = Truck.where({userId: request.user._id});
  query
      .findOne()
      .where({
        $and: [{_id: truck_id}, {$or: [{status: 'OS'}]}],
      })
      .exec()
      .then((truck) => {
        if (!truck) {
          return response.status(400).json({
            status:
            'There is no truck with such id or status is ONLOAD! Try again!',
          });
        }
        if (truck.status === 'OL') {
          return response
              .status(400)
              .json({status: 'You cannot remove truck ONLOAD'});
        }
        truck.update({$set: {assigneeId: userId, status: 'IS'}}).exec();
        response.status(200).json({message: 'Truck assigned successfully'});
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.updateUserTruckById = (request, response) => {
  const {truck_id} = request.params;
  const {type} = request.body;

  const isValid = mongoose.Types.ObjectId.isValid(truck_id);
  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }

  const query = Truck.where({userId: request.user._id});
  query
      .findOne()
      .where({
        $and: [{_id: truck_id}, {$or: [{status: 'OS'}, {status: 'IS'}]}],
      })
      .exec()
      .then((truck) => {
        if (!truck) {
          return response.status(400).json({
            status:
            'There is no truck with such id or status is ONLOAD! Try again!',
          });
        }
        if (truck.status === 'OL') {
          return response
              .status(400)
              .json({status: 'You cannot remove truck ONLOAD'});
        }
        truck.update({$set: {type: type}}).exec();
        response
            .status(200)
            .json({message: 'Truck details changed successfully'});
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.getUserTruckById = (request, response) => {
  const {truck_id} = request.params;
  const userId = request.user._id;
  const isValid = mongoose.Types.ObjectId.isValid(truck_id);
  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }
  Truck.findOne({
    $and: [
      {_id: truck_id},
      {$or: [{userId: userId}, {assigneeId: userId}]},
    ],
  })
      .exec()
      .then((truck) => {
        if (!truck) {
          return response.status(400).json({
            message:
            'There is no truck with such id or you do not have access to it',
          });
        }
        response.status(200).json({
          truck: {
            _id: truck._id,
            created_by: truck.userId,
            assigned_to: truck.assigneeId,
            type: truck.type,
            status: truck.status,
            created_date: truck.createdDate,
          },
        });
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.getUserTrucks = (request, response) => {
  const userId = request.user._id;
  Truck.find({$or: [{userId: userId}, {assigneeId: userId}]})
      .exec()
      .then((trucks) => {
        if (!trucks) {
          return response.status(400).json({message: 'You have no trucks yet'});
        }
        response.status(200).json({
          trucks: [...trucks].map((truck) => {
            return {
              _id: truck._id,
              created_by: truck.userId,
              assigned_to: truck.assigneeId,
              type: truck.type,
              status: truck.status,
              created_date: truck.createdDate,
            };
          }),
        });
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};
