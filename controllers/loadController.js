/* eslint-disable camelcase */
const Load = require('../models/load');
const mongoose = require('mongoose');
const Truck = require('../models/truck');
const {
  defineTypes,
  defineNextState,
  addLogsToLoad,
} = require('../utils/helpers');
const {loadObj, shippingObj} = require('../utils/dataConstructor');

module.exports.addLoad = (request, response) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = request.body;
  const userId = request.user._id;

  if (
    !name ||
    !payload ||
    !pickup_address ||
    !delivery_address ||
    !dimensions
  ) {
    response.status(400).json({message: 'Required params missing'});
    return;
  }
  const {width, length, height} = dimensions;
  const load = new Load({
    userId,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {width, length, height},
    logs: [{message: 'Load created successfully', time: new Date()}],
  });
  load
      .save()
      .then(() => {
        response.status(200).json({message: 'Load created successfully'});
      })
      .catch((error) => {
        response.status(400).json({message: error.message});
      });
};

module.exports.deleteUserLoadById = (request, response) => {
  const {load_id} = request.params;
  const query = Load.where({userId: request.user._id});
  query
      .findOne()
      .where({$and: [{_id: load_id}, {status: 'NEW'}]})
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(400).json({
            // eslint-disable-next-line max-len
            message: 'This load either does not exist or its status is not NEW!',
          });
        }
        load.remove();
        response.status(200).json({message: 'Load deleted successfully'});
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.getUserLoadById = (request, response) => {
  const {load_id} = request.params;
  const userId = request.user._id;
  const isValid = mongoose.Types.ObjectId.isValid(load_id);
  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }
  Load.findById({_id: load_id})
      .where({$or: [{userId: userId}, {assigneeId: userId}]})
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(400).json({
            message:
            'There is no load with such id or you do not have access to it',
          });
        }
        response.status(200).json(loadObj(load));
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.postUserLoadById = async (request, response) => {
  const {load_id} = request.params;
  const userId = request.user._id;
  const isValid = mongoose.Types.ObjectId.isValid(load_id);

  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }

  try {
    const load = await Load.findOne({
      $and: [
        {_id: load_id},
        {$and: [{userId: userId}, {status: 'NEW'}]},
      ],
    });
    if (!load) {
      return response.status(400).json({message: 'Such load does not exist'});
    }
    await Load.findByIdAndUpdate(load_id, {$set: {status: 'POSTED'}});
    const trucks = defineTypes(load.payload, load.dimensions);
    const availableTruck = await Truck.find({status: 'IS'})
        .where('type')
        .in(trucks)
        .limit(1);
    if (!availableTruck[0]) {
      await Load.findOneAndUpdate(
          {_id: load_id},
          {
            $push: {
              logs: {
                message: `No available trucks with appropriate dimentions`,
                time: Date.now,
              },
            },
          },
      );
      await Load.findByIdAndUpdate(load_id, {$set: {status: 'NEW'}});
      return response.status(400).json({
        message:
          // eslint-disable-next-line max-len
          'No available trucks with appropriate dimentions/no avaiable trucks with assignee',
      });
    }

    const id = availableTruck[0]._id;
    const truckAssignee = availableTruck[0].assigneeId;
    const message = `Load assigned to driver with id ${truckAssignee}`;

    await Truck.findByIdAndUpdate(id, {
      $set: {assigneeId: userId, status: 'OL'},
    });
    await Load.findOneAndUpdate(
        {_id: load_id},
        {
          $set: {
            status: 'ASSIGNED',
            assigneeId: truckAssignee,
            state: 'En route to Pick Up',
            truckId: availableTruck[0]._id,
          },
        },
    );
    addLogsToLoad(Load, load_id, message);
    response
        .status(200)
        .json({message: 'Load posted successfully', driver_found: true});
  } catch (err) {
    response.status(400).json({message: err.message});
  }
};

module.exports.getAllLoads = (request, response) => {
  const userId = request.user._id;
  Load.find({$or: [{userId: userId}, {assigneeId: userId}]})
      .exec()
      .then((loads) => {
        if (!loads) {
          return response
              .status(400)
              .json({message: 'You do not have any loads'});
        }
        response.status(200).json([
          {
            loads: [...loads].map((load) => {
              return loadObj(load);
            }),
          },
        ]);
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.updateUserLoadById = (request, response) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = request.body;
  const userId = request.user._id;
  const {load_id} = request.params;

  if (
    !name ||
    !payload ||
    !pickup_address ||
    !delivery_address ||
    !dimensions
  ) {
    response.status(400).json({message: 'Required params missing'});
    return;
  }

  const query = Load.where({userId: userId});
  query
      .findOne({_id: load_id})
      .exec()
      .then((load) => {
        if (!load) {
          return response
              .status(400)
              .json({message: 'There is no load with such id! Try again!'});
        }
        if (load.status !== 'NEW') {
          return res
              .status(400)
              .json({message: 'It is allowed to updated ony new loads'});
        } else if (load.status === 'NEW') {
          Load.findOneAndUpdate(
              {_id: load_id},
              {
                $set: {
                  name: name,
                  payload: payload,
                  pickup_address: pickup_address,
                  delivery_address: delivery_address,
                  dimensions: dimensions,
                },
              },
          )
              .exec()
              .then(() => {
                response
                    .status(200)
                    .json({message: 'Load details changed successfully'});
              })
              .catch((err) => {
                response.status(400).json({message: err.message});
              });
        }
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.getActiveLoads = (request, response) => {
  const userId = request.user._id;
  Load.findOne()
      .where({$and: [{assigneeId: userId}, {status: 'ASSIGNED'}]})
      .exec()
      .then((load) => {
        if (!load) {
          return response
              .status(200)
              .json({message: 'You do not have active loads'});
        }
        response.status(200).json(loadObj(load));
      })
      .catch((err) => {
        response.status(400).json({message: err.message});
      });
};

module.exports.setNextState = function(request, response) {
  const userId = request.user._id;
  Load.findOne()
      .where({$and: [{assigneeId: userId}, {status: 'ASSIGNED'}]})
      .exec()
      .then((load) => {
        if (!load) {
          return response
              .status(400)
              .json({message: 'You do not have active loads'});
        }

        const next = defineNextState(load.state, load.status);
        const message = `Load state changed to '${next.state}'`;

        if (next.status === 'SHIPPED') {
          Truck.findByIdAndUpdate(load.truckId, {
            $set: {status: 'IS'},
          }).exec();
        }

        Load.findOneAndUpdate(
            {_id: load._id},
            {
              $set: {
                state: next.state,
                status: next.status,
              },
            },
        )
            .exec()
            .then(() => {
              addLogsToLoad(Load, load._id, message);
              response.status(200).json({message: message});
            })
            .catch((err) => {
              response.status(400).json({message: err.message});
            });
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.getShippingInfo = (request, response) => {
  const {load_id} = request.params;

  const isValid = mongoose.Types.ObjectId.isValid(load_id);
  if (!isValid) {
    return response.status(400).json({
      message: 'Id format is not valid',
    });
  }

  Load.findById({_id: load_id})
      .where({userId: request.user._id})
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(400).json({
            message:
            'There is no load with such id or you do not have access to it',
          });
        }
        if (!load.truckId) {
          response.status(200).json(shippingObj(load, null));
        } else {
          Truck.findById({_id: load.truckId})
              .exec()
              .then((truck) => {
                response.status(200).json(shippingObj(load, truck));
              })
              .catch((err) => {
                response.status(400).json({message: err.message});
              });
        }
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};
