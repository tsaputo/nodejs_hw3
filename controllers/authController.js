const jwt = require('jsonwebtoken');
const User = require('../models/user');
const {createHash} = require('../utils/crypto');
require('dotenv').config();

module.exports.register = (request, response) => {
  const {email, password, role} = request.body;

  if (!email || !password || !role) {
    response.status(400).json({message: 'Required params missing'});
    return;
  }

  User.findOne({email})
      .exec()
      .then((user) => {
        if (user) {
          response
              .status(400)
              .json({message: 'User with this email already exists'});
          return;
        }

        const hash = createHash(password);
        const newUser = new User({email, password: hash, role});
        newUser
            .save()
            .then(() => {
              response
                  .status(200)
                  .json({message: 'Profile created successfully'});
            })
            .catch((error) => {
              response.status(500).json({message: error.message});
            });
      });
};

module.exports.login = (request, response) => {
  const {email, password} = request.body;

  if (!email) {
    response
        .status(400)
        .json({message: 'Email is missing. Please, provide username'});
    return;
  }

  if (!password) {
    response
        .status(400)
        .json({message: 'Password is missing. Please, enter your password'});
    return;
  }

  const hash = createHash(password);
  User.findOne({email: email, password: hash})
      .exec()
      .then((user) => {
        if (!user) {
          return response
              .status(400)
              .json({message: 'Wrong email or password'});
        }
        response.status(200).json({
          jwt_token: jwt.sign(JSON.stringify(user), process.env.JWT_SECRET),
        });
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};
