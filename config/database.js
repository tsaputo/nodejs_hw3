require('dotenv').config();

module.exports = {
  port: process.env.DB_PORT,
  host: process.env.HOST,
  databaseName: process.env.DATABASE_NAME,
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
};
